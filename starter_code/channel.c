#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "command.h"
#include "network_io.h"
#include "channel.h"
#include "client.h"
#include "replies.h"

struct channel channels[MAX_CHANNELS];

static int n_channels = 0;

int in_channel(struct channel *chan, int cli_fd)
{
	int i;

	for (i = 0; i < MAX_JOIN; i++) {
		if ((chan->joined_users[i] != NULL) && (chan->joined_users[i]->fd == cli_fd))
			return 1;
	}

	return 0;
}

struct channel *get_channel(char *chan_name)
{
	int i;

	for (i = 0; i < MAX_CHANNELS; i++)
		if (strncmp(channels[i].name, chan_name, 20) == 0)
			break;

	if (i == MAX_CHANNELS)
		return NULL;
	
	return &(channels[i]);
}

struct channel *new_channel(char *chan_name)
{
	int i;

	for (i = 0; i < MAX_CHANNELS; i++) {
		if (channels[i].n_joined == -1) /* -1 users joined means this slot in the channel array is open */
			break;
	}

	if (i == MAX_CHANNELS)
		return NULL;

	strncpy(channels[i].name, chan_name, 20);
	channels[i].n_joined = 0;

	n_channels++;

	return &(channels[i]);
}

static void remove_inactive_channels(int n)
{
	int n_removed = 0;
	int i;

	for (i = 0; i < MAX_CHANNELS; i++) {
		if (n_removed == n)
			break;
		if (channels[i].n_joined == 0) {
			memset(&channels[i], 0x00, sizeof(struct channel));
			channels[i].n_joined = -1;
			n_removed++;
		}
	}
}

static void join_channel(struct channel *chan, struct client *cli)
{
	int i, j;

	if (cli->n_joined == 1) {
			part_user(cli->joined_channels[0],cli);
			join_channel(chan,cli);
	        send_message(cli->fd, -1, "%d %s %s :You have moved to another channel", ERR_TOOMANYCHANNELS, cli->nick, chan->name);
       		return;
	}	

	for (i = 0; i < MAX_JOIN; i++) {
		if (chan->joined_users[i] == NULL) {
			chan->joined_users[i] = cli;
			chan->n_joined++;	

			/* add to clients joined channel list */
			for (j = 0; j < MAX_CHAN_JOIN; j++) {
				if (cli->joined_channels[j] == NULL) {
					cli->joined_channels[j] = chan;
					cli->n_joined++;
					break;
				}
			}

			break;
		}

	}

	if (i == MAX_JOIN) {
		send_message(cli->fd, -1, "%d %s %s :Cannot join channel (+l)", ERR_CHANNELISFULL, cli->nick, chan->name);
		return;
	}

	send_channel_greeting(chan, cli);
}

void send_channel_greeting(struct channel *chan, struct client *cli)
{
	char user_list[400];
	
	int i, n = 0;

	if (chan->topic_set_time != 0) {
		send_message(cli->fd, -1, "%d %s %s %s", RPL_TOPIC, cli->nick, chan->name, chan->topic);	
		send_message(cli->fd, -1, "%d %s %s %s %d", RPL_TOPICWHOTIME, cli->nick, chan->name, chan->topic_setter, chan->topic_set_time);
	}

	memset(user_list, 0x00, 400);

	for (i = 0; i < MAX_JOIN; i++) {
		if (chan->joined_users[i] != NULL) {
			strncat(user_list, chan->joined_users[i]->nick, 20);
			strncat(user_list, " ", 1);
			n += strlen(chan->joined_users[i]->nick + 1);
			
			if (n >= 379) { /* if maximum capacity reached, send what we have so far */
				send_message(cli->fd, -1, "%d %s = %s :%s", RPL_NAMREPLY, cli->nick, chan->name, user_list);
				memset(user_list, 0x00, 400);
				n = 0;
			}
		}
	}

	if (n > 0) {
		send_message(cli->fd, -1, "%d %s = %s :%s", RPL_NAMREPLY, cli->nick, chan->name, user_list);
		memset(user_list, 0x00, 400);
	}
}


void part_user(struct channel *chan, struct client *cli)
{
	int i;

	/* find the spot in the joined users array where the client is */
	for (i = 0; i < MAX_JOIN; i++) {
		if((chan->joined_users[i] != NULL) && (cli->fd == chan->joined_users[i]->fd))
			break;
	}

	if (i == MAX_JOIN) {
		send_message(cli->fd, -1, "%d %s %s :You are not on that channel", ERR_NOTONCHANNEL, cli->nick, chan->name);
		return;	
	}

	chan->joined_users[i] = NULL;
	chan->n_joined--;

	/* remove channel from the client list of joined_channels */
	for (i = 0; i < MAX_CHAN_JOIN; i++) {
		if (strcmp(cli->joined_channels[i]->name, chan->name) == 0) {
			cli->joined_channels[i] = NULL;
			cli->n_joined--;
			break;
		}
	}
	remove_inactive_channels(MAX_JOIN);
}

static void handle_join(int fd, int argc, char **args)
{
	struct client *cli = get_client(fd);
	struct channel *chan;
	
	if(cli->registered == 3){

		if (argc < 2) {
			send_message(fd, -1, "%d %s %s :Not enough parameters", ERR_NEEDMOREPARAMS, cli->nick, args[0]);
			return;
		}

		char *bufp = args[1];
		int i, n = 1;

		while (*bufp != '\0') { 
			if (*bufp == ',') {
				*bufp = '\0';
				n++;
			}
		
			bufp++;
		}

		bufp = args[1];
		for (i = 0; i < 1; i++) { /*i<1 is because in this project the user can only enter one channel*/
			if ((chan = get_channel(bufp)) == NULL) {
				if ((chan = new_channel(bufp)) == NULL) {
					send_message(fd, -1, "%d %s %s :No such channel/Too many channels", ERR_NOSUCHCHANNEL, cli->nick, args[0]);
					return;
				}
			}
			
			if (in_channel(chan, cli->fd)) /* can't join a channel twice, but IRC specifies no error code for this */
				continue;

			send_message(cli->fd, cli->fd, "JOIN %s", bufp);
			join_channel(chan, cli);
			send_to_channel(chan, cli->fd, "JOIN %s", bufp);

			while (*bufp != '\0') bufp++;/*these two lines might be used if the user can join multiple channels*/
			bufp++;
		}
	}
	else{
		send_message(fd,-1,"%d %s %s : Not registered",ERR_NOTREGISTERED,cli->nick,args[0]);
		return;
	}

}

static void handle_part(int fd, int argc, char **args)
{
	struct client *cli = get_client(fd);
	struct channel *chan;

	if (argc < 2) {
		send_message(fd, -1, "%d %s %s :Not enough parameters", ERR_NEEDMOREPARAMS, cli->nick, args[0]);
		return;
	}

	char *bufp;
	bufp= args[1];
	int i, n = 1;

	while (*bufp != '\0') {/*if there are several parting arguments*/
		if (*bufp == ',') {
			n++;
			*bufp = '\0';
		}

		bufp++;
	}

	bufp = args[1];

	for (i = 0; i < n; i++) {
		if ((chan = get_channel(bufp)) == NULL) {
			send_message(fd, -1, "%d %s %s :No such channel", ERR_NOSUCHCHANNEL, cli->nick, args[0]);
			return;
		}

		send_message(cli->fd, cli->fd, "PART %s %s", bufp, args[2]);
		send_to_channel(chan, cli->fd, "PART %s %s", chan->name, args[2]);
		part_user(chan, cli);


		/* advance to next channel name */
		while (*bufp != '\0') bufp++;
		bufp++;
	}
}

static void handle_list(int fd, int argc, char**args)
{
	int i,j,k;
	for (i=0; i<MAX_CHANNELS;i++){
		if(channels[i].n_joined!=-1){
			send_message(fd,-1,"CHANNEL NAME: %s",channels[i].name);
			k=0;
			for (j=0;j<MAX_JOIN;j++){
				if((channels[i].joined_users[j])!=NULL){
					// send_message(fd,-1,"USER NAME: %s",(channels[i].joined_users[j]->nick));
					k++;
				}
			}
			send_message(fd,-1,"NUMBER OF USERS: %d",k);
		}
	}
}

static void handle_privmsg(int fd, int argc, char**args){
	struct client  *cli = get_client(fd);
	struct channel *target_chan;
	struct client *target_cli;

	char *bufp = args[1];
	int is_channel = 1;

	if (argc < 3) {
		send_message(fd, -1, "%d %s :No text to send", ERR_NOTEXTTOSEND, cli->nick);
		return;
	}
	if (argc < 2) {
		send_message(fd, -1, "%d %s :No recipient given (PRIVMSG)", ERR_NORECIPIENT, cli->nick);
		return;
	}
	if ((target_chan = get_channel(bufp)) == NULL) {
		if ((target_cli = get_client_nick(bufp)) == NULL) {
			send_message(fd, -1, "%d %s %s :No such nick/channel", ERR_NOSUCHNICK, cli->nick, bufp);
			return;
		}
		is_channel = 0;
	}

	if (is_channel && !in_channel(target_chan, cli->fd)) {
		send_message(fd, -1, "%d %s %s :Cannot send to channel", ERR_CANNOTSENDTOCHAN, cli->nick, bufp);
		return;
	}
	if ((target_chan = get_channel(bufp)) != NULL) {
		send_to_channel(target_chan, cli->fd, args[2]);
	}
	if ((target_cli = get_client_nick(bufp)) != NULL){
		send_message(target_cli->fd, cli->fd, args[2]);
	}
}


static void handle_who(int fd, int argc, char**args){
	struct client *cli = get_client(fd);
	struct channel *chan=get_channel(args[1]);
	if(argc<2){
		send_message(fd,-1,"%d %s %s :Not enough parameters", ERR_NEEDMOREPARAMS,cli->nick,args[0]);
		return;
	}

	if (chan == NULL){
		send_message(fd,-1,"%d, %s, %s :No such channel", ERR_NOSUCHCHANNEL,cli->nick,args[0]);
		return;
	}

	int j;
		for (j=0;j<MAX_JOIN;j++){
		if((chan->joined_users[j])!=NULL){
			send_message(fd,-1,"USER NAME: %s",(chan->joined_users[j]->nick));
		}

	}
	return;
}

void initialize_channels()
{
	int i;

	memset(&channels, 0x00, sizeof(struct channel) * MAX_CHANNELS);

	for (i = 0; i < MAX_CHANNELS; i++)
		channels[i].n_joined = -1;

	register_command("JOIN", handle_join);
	register_command("PART", handle_part);
	register_command("LIST", handle_list);
	register_command("PRIVMSG",handle_privmsg);
	register_command("WHO",handle_who);

}